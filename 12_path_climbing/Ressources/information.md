# Path Traversal

## Usage

Simply navigate in the server using `../` in the `$ip/?page=`
for example:
	* `$ip/?page=../../../../../../../etc/passwd`

This fail is a huge one because someone can access to any creditential of the
website or the server.

## Protection

* Ideally, application functionality should be designed in such a way that user-controllable
data does not need to be placed into file or URL paths in order to access local
resources on the server. This can normally be achieved by referencing known
files via an index number rather than their name.

* If it is considered unavoidable to place user data into file or URL paths,
the data should be strictly validated against a whitelist of accepted values.


## More on the Subject...
[CWE Tips - Path Traversal](https://cwe.mitre.org/data/definitions/35.html)
[CWE Tips - Improprer Limitation](https://cwe.mitre.org/data/definitions/22.html)
[OWASP Cheat Sheet](https://www.owasp.org/index.php/Path_Traversal)
