# Brute Force Sign In 1 - User Login

## Usage

Go to the sign in page and try to connect with the most common login information.
In this case, we used a seclist of the [25 most used password](https://github.com/danielmiessler/SecLists/blob/master/Passwords/Common-Credentials/top-passwords-shortlist.txt),
with 'Admin' as the user login.

We managed to find the following Credentials :
* Login: **admin**
* Password: **shadow**

This is a big issue, because anyone can access the administrator private page.

## Protection

* When possible, implement  multi-factor authentication .
* Do not use default credentials.
* Use a better login and a strong password, and test them by comparing with the available seclists.
* Ensure registration, credential recovery, and API pathways are hardened against account enumeration attacks by using the same messages for all outcomes.

## More on the Subject...
* [OWASP Guide on Brute Force](https://www.owasp.org/index.php/Brute_force_attack)
* [Actualised Credentials Seclists](https://github.com/danielmiessler/SecLists)
