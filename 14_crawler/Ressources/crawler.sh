#!/bin/sh

#STEP 0: Remplacer <YOUR_VM_IP> par l'IP du site web !
USAGE="Usage: crawler.sh \"YOUR_VM_IP"

function checkIp() {
	ping -c1 $1 &> /dev/null

	if [ $? -eq 0 ]
	then
		theCrawl $1;
	else
		echo "IP not reachable, please providea valid IP"
		exit
	fi
}

function theCrawl() {
	echo "Searching through : $1/.hidden"
	echo "Please be patient"

	# -----------------------------------------------
	# STEP 1 : wget du dossier hidden, en récursif et sans les dossiers parents.
	# -e robots=off pour que les disallow du fichier robots.txt ne soient pas pris en compte
	# le dossier ./hidden n'apparait pas dans le finder, c'est un dossier caché.
	# -----------------------------------------------

	wget -q -e robots=off -r -np http://$1/.hidden/

	# -----------------------------------------------
	# STEP 2 : Find pour trouver un fichier avec un contenu qui ressemble à un flag (regex avec un pattern chiffres+lettres)
	# -----------------------------------------------

	find ./$1/.hidden -name 'README' -exec sh -c "cat {} | grep '\([a-zA-Z]*\d\+[a-zA-Z]*\)'" \; > flag.txt;
	echo "The Flag is:";
	cat flag.txt;

	# -----------------------------------------------
	# STEP 3 : Obtenir le path du fichier
	# -----------------------------------------------

	echo "Path to file containing the Flag :";
	grep -Rl `cat flag.txt` ./$1/.hidden/;
}

if [ $# -ne 1 ]
then
	echo "Wrong number of arguments. The script should be used with one and only one arg!"
	echo $USAGE
	exit
elif [ -z "$1" ]
then
	echo "Wrong argument. The arg should not be an empty string !"
	echo $USAGE
	exit
else
	checkIp $1;
	rm -rf $1;
fi
