#!/bin/sh


USAGE="Usage: header_modify.sh \"YOUR_VM_IP"



if [ $# -ne 1 ]
then
  echo "Wrong number of arguments. The script should be used with one and only one arg!"
  echo $USAGE
  exit
elif [ -z "$1" ]
  then
    echo "Wrong argument. The arg should not be an empty string !"
    echo $USAGE
	exit
fi

curl -s -A 'ft_bornToSec' --referer "https://www.nsa.gov/" "$1/?page=e43ad1fdc54babe674da7c7b8f0127bde61de3fbe01def7d00f151c2fcca6d1c" | grep 'flag'
