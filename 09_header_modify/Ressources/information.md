# Header Modify - User Agent Spoofing

## Usage

In the page *$IP/?page=e43ad1fdc54babe674da7c7b8f0127bde61de3fbe01def7d00f151c2fcca6d1c*,
if you look at source code you will find some comments, in this comment there is indication
on changing your __User Agent__ or your __referer__.

Use extension on your browser to change the __User Agent__ or __referer__.
You could also use the provided script that use curl.
Alternatively, Burp's inspector tool is quite useful to modify this easily.

This kind of hack is quite used for spamming !
By serving spammy content only to users with specific user-agents or referrers,
the hacker can target more "real people", and not the bots, and can better avoid detection from
site owners and anti-hacking algorithms used by search engines.

Also, knowing the user agent of some user may give clues about their OS,
and their social profile. Some [websites did used this at their benefit](https://www.journaldugeek.com/2012/06/26/vous-avez-un-mac-vous-etes-forcement-riche/)!

It might also allow a user to avoid some restrictions from the webmaster
based on the defaul user-agent.

## Protection

Just have a better admin auth system, and if it's an API add a token to the
process to make it more secure.

## More on the subject
 * [Quite Interesting Notions](https://aw-snap.info/file-viewer/)
 * [Google Forum](https://support.google.com/webmasters/forum/AAAA2Jdx3sUOtdabIHkmmk/?hl=en&gpf=%23!topic%2Fwebmasters%2FOtdabIHkmmk%3Bcontext-place%3Dtopicsearchin%2Fwebmasters%2Fuser-agent%2420spoofing%2420hack)
 * [Spoofing and Privacy](https://tutox.fr/2019/07/05/spoofer-en-2-secondes-son-useragent-sous-firefox/)
