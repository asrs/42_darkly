# Hidden Field

## Usage

Go to *$IP/?page=recover*. In the sign in page, you will find a form with an hidden field.
Just change the value of the hidden field (the webmaster's email address)
with your own mail, so it will send the admin password, to the indicated email address.

## Protection

* Don't put the mail in the html page, you should do it in the Back End of the website.
* Use a Reset Token instead of creating and storing the password.

## More on the Subject...
* [OWASP Consideration about Password Reset](https://cheatsheetseries.owasp.org/cheatsheets/Forgot_Password_Cheat_Sheet.html)
* [Tips about How to Implement Reset Password](https://www.meziantou.net/how-to-implement-password-reset-feature-in-a-web-application.htm)
* [Introduction to Passzord Reset Poisoning](https://www.acunetix.com/blog/articles/password-reset-poisoning/)
