# Phishing Open Redirection

## Usage

There some icon for social media at the bottom of the page.

if we look for each icon the <a> balise it contains, the `href` give us this:
	```
	$ip/?page=redirect&site=facebook
	```

Simply change the `site=` value to redirect to any website of your choice.

This is a huge issue, because it allow a hacker to use a valid link to redirect
to any website of his choice.

Imagine that your banks got this issue, and someone send through email a fraudulous
link with this valid URL.

You will almost have no way to know this is a fraudulous URL.

## Protection

* Maintain a server-side list of all URLs that are permitted for redirection.
Instead of passing the target URL as a parameter to the redirector, pass an index into this list.

* Remove the redirection function from the application, and replace links to it with direct links to the relevant target URLs.

* If it is considered unavoidable for the redirection function to receive user-controllable input and incorporate this into the redirection target,
one of the following measures should be used to minimize the risk of redirection attacks:
   1. The application should use relative URLs in all of its redirects, and the redirection function should strictly validate that the URL received is a relative URL.
   2. The application should use URLs relative to the web root for all of its redirects, and the redirection function should validate that the URL received starts with a slash character. It should then prepend http://yourdomainname.com to the URL before issuing the redirect.
	3. The application should use absolute URLs for all of its redirects, and the redirection function should verify that the user-supplied URL begins with http://yourdomainname.com/ before issuing the redirect.

## More on the Subject...
[CWE Tips](https://cwe.mitre.org/data/definitions/601.html)
