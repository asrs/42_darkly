# Brute Force Sign In 2 - Admin Section

## Usage

By testing the *$IP/* page, with different default suffixes, we find one working :
*$IP/admin/* .

We applied the Brute Force technique on it to, with the same list.
It didn't work for the 'Admin' login, so we used [other common username](https://github.com/danielmiessler/SecLists/blob/master/Usernames/top-usernames-shortlist.txt),
and we managed to break it with the 'root' username.

We managed to find the following Credentials :
* Login: **root**
* Password: **dragon**

Later in the subject, we found two other ways to break this credential:
1. Using the robots.txt file, we found a hidden directory called **whatever**,
containing a file **htpassword**. In plain text in this file, you can find the
credentials for the Admin Section.
2. Using SQL Injection, both the last two credentials informations could be found
in the db_default table ! (Try to use this command : *0 OR 1 = 1 UNION SELECT username, password FROM Member_Brute_Force.db_default*)

## Protection

* Do not rely on robots.txt to provide any kind of protection over unauthorized access.

And as in the previous flag :
* When possible, implement  multi-factor authentication .
* Do not use default credentials.
* Use a better login and a strong password, and test them by comparing with the available seclists.
* Ensure registration, credential recovery, and API pathways are hardened against account enumeration attacks by using the same messages for all outcomes.

## More on the Subject...
* [OWASP Guide on Brute Force](https://www.owasp.org/index.php/Brute_force_attack)
* [Actualised Credentials Seclists](https://github.com/danielmiessler/SecLists)
