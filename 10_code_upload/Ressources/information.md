# Content Type Modification

## Usage

On the page *$ip/?page=upload*, you can upload pictures to the website.
If you try with a random .jpeg file, you'll see that the picture is indeed uploaded
correctly. But if you try to send another type of file that is not a picture,
your upload will fail.

In fact, what is taken into account is the content-type about the file contained
in the header of the request, and not the actual file. Using a software such
as Burp, or another web proxy, you can easily intercept and modify the sended file
and upload a script for instance, as long as you put image/jpeg in the content type info.


## Protection

1. **Create a new file name**
    Create your own unpredictable file name.

2. **Store the file outside of your document root**
    That way, an attacker will not be able to retrieve the file directly.

3. **Check the file size**
    Make sure to check the file size after the upload completed. Be in particular
    careful if you allow the upload of compressed files and later uncompress them
     on the server. This scenario is very hard to secure.

4. **Extensions are meaningless**
    The extension isn't what matters, but the Content-Type header and the file's header.
    It is best to use the "file" command on unix to check the file type. But even this is not fool proof...

5. **Try a malware scan**
    Even if everything looks fine, you never know.

6. **Keep tight control of permissions**
    After the file is downloaded, you could apply additional restrictions if this is appropriate.
    Sometimes it can be helpful to remove the execute permission

7. **Authenticate file uploads**
    Allows to track who sended it.

8. **Limit the number of uploaded files**
    Protection against DDoS Attacks.

## More on the subject
* [Unrestricted Upload](https://cwe.mitre.org/data/definitions/434.html)
* [Main Upload Attacks](https://www.hackingarticles.in/5-ways-file-upload-vulnerability-exploitation/)
* [OWASP Cheat Sheet](https://cheatsheetseries.owasp.org/cheatsheets/Protect_FileUpload_Against_Malicious_File.html)
* [Rules to implement](https://software-security.sans.org/blog/2009/12/28/8-basic-rules-to-implement-secure-file-uploads)
