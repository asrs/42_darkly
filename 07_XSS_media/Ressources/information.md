# XSS 2 - Media

## Usage

From the Home Page, you can notice that only picture is clickable, the one with
the NSA Logo. You then reach the following page: *$IP/index.php?page=media&src=nsa*.

We can use the '*src*' value to change the content of the downloaded page, and instead
upload some text containint a script !

After some tests, we used the following to and obtain the flag :
*$IP/index.php?page=media&src=data:text/html;base64,PHNjcmlwdD5hbGVydCgnWFNTJyk8L3NjcmlwdD4K*

## Protection (same as previous flag)

Using frameworks that automatically escape XSS by design, such as the latest Ruby on Rails, React JS.
Learn the limitations of each framework's XSS protection and appropriately handle the use cases which are not covered

The 3 main method to protect from XSS are the following :

#### 1. Escaping

 The first method you can and should use to prevent XSS vulnerabilities from appearing in your applications is by escaping user input. Escaping data means taking the data an application has received and ensuring it’s secure before rendering it for the end user. By escaping user input, key characters in the data received by a web page will be prevented from being interpreted in any malicious way. In essence, you’re censoring the data your web page receives in a way that will disallow the characters – especially < and > characters – from being rendered, which otherwise could cause harm to the application and/or users.

  If your page doesn’t allow users to add their own code to the page, a good rule of thumb is to then escape any and all HTML, URL, and JavaScript entities. However, if your web page does allow users to add rich text, such as on forums or post comments, you have a few choices.  You’ll either need to carefully choose which HTML entities you will escape and which you won’t, or by using a replacement format for raw HTML such as Markdown, which will in turn allow you to continue escaping all HTML.

#### 2. Validating Input

As Troy Hunt so eloquently puts it: “The theory goes like this: Expect any untrusted data to be malicious. What’s untrusted data?  Anything that originates from outside the system and you don’t have absolute control over so that includes form data, query strings, cookies, other request headers, data from other systems (i.e. from web services) and basically anything that you can’t be 100% confident doesn’t contain evil things.”

Validating input is the process of ensuring an application is rendering the correct data and preventing malicious data from doing harm to the site, database, and users. While whitelisting and input validation are more commonly associated with SQL injection, they can also be used as an additional method of prevention for XSS. Whereas blacklisting, or disallowing certain, predetermined characters in user input, disallows only known bad characters, whitelisting only allows known good characters and is a better method for preventing XSS attacks as well as others.

Input validation is especially helpful and good at preventing XSS in forms, as it prevents a user from adding special characters into the fields, instead refusing the request. However, as OWASP maintains,  input validation is not a primary prevention method for vulnerabilities such as XSS and SQL injection, but instead helps to reduce the effects should an attacker discover such a vulnerability.

#### 3. Sanitizing

A third way to prevent cross-site scripting attacks is to sanitize user input. Sanitizing data is a strong defense, but should not be used alone to battle XSS attacks. It’s totally possible you’ll find the need to use all three methods of prevention in working towards a more secure application. Sanitizing user input is especially helpful on sites that allow HTML markup, to ensure data received can do no harm to users as well as your database by scrubbing the data clean of potentially harmful markup, changing unacceptable user input to an acceptable format.

## More on the Subject...
* [OWASP Cheat Sheet](https://cheatsheetseries.owasp.org/cheatsheets/Cross_Site_Scripting_Prevention_Cheat_Sheet.html)
* [XSS Examples](https://github.com/swisskyrepo/PayloadsAllTheThings/tree/master/XSS%20Injection#data-grabber-for-xss)
